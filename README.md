This assumes you are installing a portal instance on a machine in a Merge experiment.

Preflight:
* create experiment on merge with one node
* materialize
* create XDC and attach to materializations
* clone this repo onto the XDC

Configure the setup:
* edit ./inventory to match your experiment node name
* edit extra-vars.yml to your list of users. Users in a project can be shown via:
    `mrg show project $PROJECT -j | jq -r '.project.members | keys[]'`
* run the ansible: `ansible-playbook -i inventory -e @extra-vars.yml portal.yml`
* ssh to the experiment node
* clone https://gitlab.com/mergetb/devops/vte/portal/minikube-portal
* cd into `minikube-portal`
* run `./start-portal`

Notes:
* minikube commands are available on the node
* kubectl is installed on the node
* `portalops` and `testuser` accounts are created on the portal. passwords are `k4tBack!` by default
* a 4-node facility is setup for resources (but does not exist - so no materalizations)
* ssh to XDCs does not work.
* minikube profile name is `portal`
